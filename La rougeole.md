# La rougeole
Pour ce projet, nous avons travaillé sur le virus de la rougeole. Notre but était
d'informer et de prévenir ce virus. Nous avons décidé de réaliser quatre simulations
que nous avons ajoutées par la suite à un [site web](https://llleonie19.github.io/rougeole.html) : 
- la [première simulation](https://gitlab.com/Kily_/la-rougeole/-/blob/master/simulations%20python/graphiques.html)
est un graphique représentant l'évolution de la rougeole en Europe entre 2018 et 2019. 

- la [deuxième simulation](https://gitlab.com/Kily_/la-rougeole/-/blob/master/Explications%20/Simulation%201.md) 
montre le fonctionnement des simulations suivantes. 

- les [deux dernières simulations](https://gitlab.com/Kily_/la-rougeole/-/blob/master/Explications%20/Simulation%202%20et%203.md) 
montrent les effets de la dispersion du virus sur une population sans aucunes mesures 
prises pour l'arrêter, puis sur une population vaccinée.