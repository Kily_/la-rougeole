Tout d'abord, nous procédons aux importations des bibliothèques.  

```python
import turtle
import random as rd
from math import *
```
Nous travaillons avec l'interface graphique `turtle`.  
`random`est une bibliothèque qui regroupe des fonctions permettant de simuler le 
hasard.  
`math` est une bibliothèque qui permet d’avoir accès aux fonctions mathématiques.

Puis, nous créons la fenêtre.
```python
wn = turtle.Screen()
wn.bgcolor("white")
wn.title("Simulation Rougeole 1")
wn.tracer(0)
```

Ensuite, nous mettons les listes et les variables. 
```python
balles = []
contamine = []
nbIndividu = 30
pourcentageContamine = 0.9
contamineMax = (int)(nbIndividu*pourcentageContamine)
```
La liste `balles` contiendra l'ensemble des balles tandis que la liste `contamine`
contiendra les individus contaminés.  
Nous avons choisi de simuler une population contenant 30 individus. Celle-ci contient
10% de gens immunisés naturellement contre le virus. 90% des gens sont donc susceptibles 
d'être contaminés.  
Pour l'autre simulation, 90% de la population est vaccinée, seulement
10% des gens sont donc susceptible d'être infecté.  
La variable `contamineMax` nous sert à calculer le nombre maximum de gens qui seront
contaminés. Pour cela on multiplie le nombre d'individus par le pourcentage de la population
contaminée.

Puis, nous créons une fonction.
```python
def verifPos(balle, ind):
    found = 'false'
    i = 0 
    for b in balles:
        if balle.xcor() <= b.xcor()+15 and balle.xcor() >= b.xcor()-15 and balle.ycor() <= b.ycor()+15 and balle.ycor() >= b.ycor()-15 and i != ind:
            if(b.color()[0] == 'red' and balle.color()[0] == 'blue') and len(contamine) < contamineMax :
                balle.color('red')
                contamine.append(balle)
            found = 'true'
        i += 1
        if balle.xcor() > 300 or balle.xcor() < -300:
            found = 'x'
        if balle.ycor() > 300 or balle.ycor() < -300:
            found = 'y'
    return (found)
```
Cette fonction nous permet de vérifier si les balles rebondissent entre elles et
sur mes parois. Ici, les limites de la case sont -300 et 300 et le diamètre d'une
balle est 15.

Ensuite, nous créons les balles que nous ajoutons à la liste `balles`.

```python
for _ in range(nbIndividu):
    #Initialisation de la balle
    t = turtle.Turtle()
    t.shape("circle")
    t.color('blue')
    t.penup() # permet de ne pas avoir de dessin
    t.speed(0) # vitesse de l'animation
    x = rd.randint(-300, 300) # abscisse 
    y = rd.randint(-300, 300)  # ordonnées  
    t.goto(x,y) # déplacement de la balle vers ses coordonnées (x,y)
    t.dy =rd.randint(-4, 4) # vitesse
    t.dx =rd.randint(-4, 4) # vitesse

    #Ajout de la balle à la liste
    balles.append(t)
```
Puis,nous changeons la couleur de la balle.
```python
cpt = 0
cp = 0
while True:
    if cp == 100 :
        balles[3].color('red')
        contamine.append(balles)
    wn.update()
    ind = 0
    if cpt%100 == 0 :
        indice = (int)(cpt/100)
        if indice < len(contamine) :
            contamine[indice].color('grey')
        else:
            cpt = 0
```
les variables `cpt` et `cp` sont des compteurs. Lorsque `cp` est arrivé à 100, une
balle devient rouge (infectées). Lorsque `cpt`/100 = 0, les balles deviennent grises 
(guérit).

Ensuite, nous faisont bouger les balles. 
```python
for balle in balles:  
        balle.sety(balle.ycor() + balle.dy)  # changement de y 
        balle.setx(balle.xcor() + balle.dx) # changement de x


        # Vérifier collision avec autre balles et parrois 
        verif = verifPos(balle, ind)
        if(verif == 'true'):
            #balle.dx *= -1
            balle.dy *= -1

        if(verif == 'x'):
            balle.dx *= -1

        if(verif == 'y'):
            balle.dy *= -1
        
        ind += 1
    cpt += 1
     cp += 1
```
Lorsque le rebond est vérifié, les balles partent dans les sens opposés et on rajoute
1 aux compteurs. 

Enfin, nous affichons la simulation.
```python
wn.mainloop()
```
retrouvez le programme entier [ici](https://gitlab.com/Kily_/la-rougeole/-/blob/master/simulations%20python/simulation%203%20et%204.py)
   