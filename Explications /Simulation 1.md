La première simulation est pareille que les deux suivantes, nous avons
cepandant enlevé tout ce qui se rapportait aux ordonnées, ralentit l'animation et
réduit la population à 5 individus.  
Nous avons expliqué la simulation en détail [ici](https://gitlab.com/Kily_/la-rougeole/-/blob/master/Explications%20/Simulation%202%20et%203.md).  
Retrouvez le programme en entier [ici](https://gitlab.com/Kily_/la-rougeole/-/blob/master/simulations%20python/simulation%201%20et%202.py).