import turtle
import random as rd
import time
from math import *

# la fenêtre
wn = turtle.Screen()
wn.bgcolor("white")
wn.title("Simulation Rougeole 1")
wn.tracer(0)

balles = []
contamine = []
nbIndividu = 30
pourcentageContamine = 0.9
contamineMax = (int)(nbIndividu*pourcentageContamine)

def verifPos(balle, ind):
""" 
Vérifie le rebond d'une balles lorsqu'elle atteint les limites de la case ou 
lorsqu'elle rentre en contact avec une autre balle
"""
    found = 'false'
    i = 0 
    for b in balles:
        if balle.xcor() <= b.xcor()+15 and balle.xcor() >= b.xcor()-15 and balle.ycor() <= b.ycor()+15 and balle.ycor() >= b.ycor()-15 and i != ind:
            if(b.color()[0] == 'red' and balle.color()[0] == 'blue') and len(contamine) < contamineMax :
                balle.color('red')
                contamine.append(balle)
            found = 'true'
        i += 1
        if balle.xcor() > 300 or balle.xcor() < -300:
            found = 'x'
        if balle.ycor() > 300 or balle.ycor() < -300:
            found = 'y'
    return (found)



# Rajouter plein de balles
for _ in range(nbIndividu):
    #Initialisation de la balle
    t = turtle.Turtle()
    t.shape("circle")
    t.color('blue')
    t.penup() # permet de ne pas avoir de dessin
    t.speed(0) # vitesse de l'animation
    x = rd.randint(-300, 300) # abscisse 
    y = rd.randint(-300, 300)  # ordonnées  
    t.goto(x,y) # déplacement de la balle vers ses coordonnées (x,y)
    t.dy =rd.randint(-4, 4) # vitesse
    t.dx =rd.randint(-4, 4) # vitesse

    #Ajout de la balle à la liste
    balles.append(t)

cpt = 0
cp = 0
while True:
    if cp == 100 :
        balles[3].color('red')
        contamine.append(balles)
    wn.update()
    ind = 0
    if cpt%100 == 0 :
        indice = (int)(cpt/100)
        if indice < len(contamine) :
            contamine[indice].color('grey')
        else:
            cpt = 0

    for balle in balles:  
        balle.sety(balle.ycor() + balle.dy)  # changement de y 
        balle.setx(balle.xcor() + balle.dx) # changement de x


        # Vérifier collision avec autre balles et parrois 
        verif = verifPos(balle, ind)
        if(verif == 'true'):
            #balle.dx *= -1
            balle.dy *= -1

        if(verif == 'x'):
            balle.dx *= -1

        if(verif == 'y'):
            balle.dy *= -1
        
        ind += 1
    cpt += 1
    cp += 1

wn.mainloop()